import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, CKEditorModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.less',
})
export class AppComponent implements OnInit {
  title = 'test-editor';
  public Editor: any = null;

  ngOnInit(): void {
    import('../assets/ckeditor/ckeditor.js')
      .then((editor) => {
        this.Editor = editor.default;
      })
      .catch(() => {
        this.Editor = undefined;
      });
  }
}
