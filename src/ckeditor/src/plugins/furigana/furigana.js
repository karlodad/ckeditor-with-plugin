/**
 * @license Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see LICENSE.md.
 */

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import FuriganaEditing from './furiganaediting';
import FuriganaUI from './furiganaui';

export default class Furigana extends Plugin {
  static get requires() {
    return [FuriganaEditing, FuriganaUI];
  }
}
