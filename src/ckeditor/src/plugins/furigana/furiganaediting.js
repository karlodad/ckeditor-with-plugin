/**
 * @license Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see LICENSE.md.
 */

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import FuriganaCommand from './furiganacommand';

export default class FuriganaEditing extends Plugin {
  init() {
    this._defineSchema();
    this._defineConverters();

    this.editor.commands.add('addFurigana', new FuriganaCommand(this.editor));
  }
  _defineSchema() {
    const schema = this.editor.model.schema;

    schema.extend('$text', { allowAttributes: ['furigana'] });
  }
  _defineConverters() {
    const conversion = this.editor.conversion;

    conversion.for('downcast').attributeToElement({
      model: 'furigana',
      view: (modelAttributeValue, conversionApi) => {
        const { writer } = conversionApi;
        return writer.createAttributeElement('span', {
          'data-furigana': modelAttributeValue,
        });
      },
    });

    conversion.for('upcast').elementToAttribute({
      view: {
        name: 'span',
        attributes: ['data-furigana'],
      },
      model: {
        key: 'furigana',
        value: (viewElement) => {
          const title = viewElement.getAttribute('data-furigana');
          return title;
        },
      },
    });
  }
}
