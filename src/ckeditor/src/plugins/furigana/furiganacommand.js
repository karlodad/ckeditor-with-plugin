/**
 * @license Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see LICENSE.md.
 */

import Command from '@ckeditor/ckeditor5-core/src/command';
import findAttributeRange from '@ckeditor/ckeditor5-typing/src/utils/findattributerange';
import getRangeText from './utils.js';
import { toMap } from '@ckeditor/ckeditor5-utils';

export default class FuriganaCommand extends Command {
  refresh() {
    const model = this.editor.model;
    const selection = model.document.selection;
    const firstRange = selection.getFirstRange();

    if (firstRange.isCollapsed) {
      if (selection.hasAttribute('furigana')) {
        const attributeValue = selection.getAttribute('furigana');
        const furiganaRange = findAttributeRange(selection.getFirstPosition(), 'furigana', attributeValue, model);
        this.value = { abbr: getRangeText(furiganaRange), title: attributeValue, range: furiganaRange };
      } else {
        this.value = null;
      }
    } else {
      if (selection.hasAttribute('furigana')) {
        const attributeValue = selection.getAttribute('furigana');
        const furiganaRange = findAttributeRange(selection.getFirstPosition(), 'furigana', attributeValue, model);
        if (furiganaRange.containsRange(firstRange, true)) {
          this.value = { abbr: getRangeText(firstRange), title: attributeValue, range: firstRange };
        } else {
          this.value = null;
        }
      } else {
        this.value = null;
      }
    }
    this.isEnabled = model.schema.checkAttributeInSelection(selection, 'furigana');
  }

  execute({ abbr, title }) {
    const model = this.editor.model;
    const selection = model.document.selection;

    model.change((writer) => {
      if (selection.isCollapsed) {
        if (this.value) {
          const { end: positionAfter } = model.insertContent(
            writer.createText(abbr, { abbreviation: title }),
            this.value.range,
          );
          writer.setSelection(positionAfter);
        } else if (abbr !== '') {
          const firstPosition = selection.getFirstPosition();
          const attributes = toMap(selection.getAttributes());
          attributes.set('furigana', title);
          const { end: positionAfter } = model.insertContent(writer.createText(abbr, attributes), firstPosition);
          writer.setSelection(positionAfter);
        }
        writer.removeSelectionAttribute('furigana');
      } else {
        const ranges = model.schema.getValidRanges(selection.getRanges(), 'furigana');
        for (const range of ranges) {
          writer.setAttribute('furigana', title, range);
        }
      }
    });
  }
}
